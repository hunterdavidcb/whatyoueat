﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(Item))]
public class ItemInspector : Editor
{
	public override void OnInspectorGUI()
	{
		//base.OnInspectorGUI();
		Item item = target as Item;
		for (int i = 0; i < item.traits.Count; i++)
		{

			EditorGUI.BeginChangeCheck();
			item.traits[i] = EditorGUILayout.ObjectField(item.traits[i],typeof(NPCTrait),false) as NPCTrait;
			if (EditorGUI.EndChangeCheck())
			{
				EditorUtility.SetDirty(item);
			}

			EditorGUI.BeginChangeCheck();
			item.ranges[i] = EditorGUILayout.Vector2Field("Range", item.ranges[i]);
			if (EditorGUI.EndChangeCheck())
			{
				EditorUtility.SetDirty(item);
			}

			if (GUILayout.Button("Remove trait"))
			{
				item.traits.RemoveAt(i);
				item.ranges.RemoveAt(i);
				EditorUtility.SetDirty(item);
			}

			EditorGUILayout.Separator();
		}

		if (GUILayout.Button("Add trait"))
		{
			item.traits.Add(null);
			item.ranges.Add(new Vector2());
			EditorUtility.SetDirty(item);
		}
	}
}
