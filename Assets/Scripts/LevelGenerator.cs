﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelGenerator : MonoBehaviour
{
	public GameObject roundGrass;
	public GameObject squareGrass;
	public GameObject hill;
	public GameObject mountain;

	float xSize;
	float zSize;
    // Start is called before the first frame update
    void Start()
    {
		xSize = squareGrass.GetComponent<BoxCollider>().size.x - 2f ;
		zSize = squareGrass.GetComponent<BoxCollider>().size.z - 2f;
		//Debug.Log(xSize);
		//Debug.Log(zSize);

		//for (int x = 0; x < 20; x++)
		//{
		//	for (int z = 0; z < 20; z++)
		//	{
		//		bool isEven = z % 2 == 0;
		//		//				//Debug.Log(z + r);
		//		///				Debug.Log(i + q/7);
		//		//				//Debug.Log("z " + z + " r" + r + " " +isEven);
		//						Instantiate(squareGrass, new Vector3(isEven ? (x * xSize) : (x + .5f) * xSize
		//							, 0f, z * zSize), Quaternion.identity);
		//	}
		//}

		for (int x = 0; x < 100; x+=7)
		{
			for (int z = 0; z < 100; z+=7)
			{
				bool isHill = Random.Range(1, 8) >= 6 && x + 4 >= 0 && z + 4 >= 0;
				if (isHill)
				{
					Instantiate(hill, new Vector3((x + 3f) * xSize, 0f, (z + 3f) * zSize), Quaternion.identity);
				}
				else
				{
					for (int xx = 0; xx < 7; xx++)
					{
						for (int zz = 0; zz < 7; zz++)
						{
							bool isEven = (z + zz) % 2 == 0;
							//				//Debug.Log(z + r);
							///				Debug.Log(i + q/7);
							//				//Debug.Log("z " + z + " r" + r + " " +isEven);
							Instantiate(squareGrass, new Vector3(isEven ? ((x + xx) * xSize) : (x + xx + .5f) * xSize
								, 0f, (z + zz) * zSize), Quaternion.identity);
						}
					}
				}
				
			}
		}
	}

    // Update is called once per frame
    void Update()
    {
        
    }



}
