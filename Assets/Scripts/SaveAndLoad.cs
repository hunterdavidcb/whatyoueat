﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public static class SaveAndLoad
{
	public static void Save(GameSaveData pd)
	{

		Debug.Log(pd.playerPositionX + " " + pd.playerPositionY + " " + pd.playerPositionZ);
		Debug.Log(pd.playerRotationX + " " + pd.playerRotationY + " " + pd.playerRotationZ);
		foreach (var ti in pd.playerTraits)
		{
			Debug.Log(ti.Key + " " + ti.Value.Value);
		}

		BinaryFormatter bf = new BinaryFormatter();

		FileStream s = File.Create(Application.persistentDataPath + "/SaveData.dat");

		bf.Serialize(s, pd);

		s.Close();
	}

	public static GameSaveData Load()
	{
		BinaryFormatter bf = new BinaryFormatter();

		FileStream s = File.Open(Application.persistentDataPath + "/SaveData.dat",FileMode.Open);

		GameSaveData pd;
		pd = (GameSaveData)bf.Deserialize(s);
		Debug.Log("loading");
		Debug.Log(pd.playerPositionX + " " + pd.playerPositionY + " " + pd.playerPositionZ);
		Debug.Log(pd.playerRotationX + " " + pd.playerRotationY + " " + pd.playerRotationZ);
		foreach (var ti in pd.playerTraits)
		{
			Debug.Log(ti.Key + " " + ti.Value.Value);
		}

		s.Close();
		return pd;
	}
}
