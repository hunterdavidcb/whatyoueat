﻿using UnityEngine;
using System.Collections.Generic;
using System;


[Serializable]
public class GameSaveData
{
	public float playerPositionX;
	public float playerPositionY;
	public float playerPositionZ;
	public float playerRotationX;
	public float playerRotationY;
	public float playerRotationZ;
	public Dictionary<string, TraitInstance> playerTraits;
		
}
