﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class PlayerController : MonoBehaviour
{
	float jumpHeight; // meters
	float fallMultiplier = 2.5f;
	float lowJumpMultiplier = 2f;

	Rigidbody rb;

	float lift; //in kg
	float moveSpeed; // meters per second
	float turnSpeed; // degrees per second
	float breathLength; // seconds
	float runTime; // seconds

	bool isGrounded = false;
	new Collider collider;

	public delegate void ItemConsumptionHandler(Dictionary<string, TraitInstance> traits);
	public event ItemConsumptionHandler ItemConsumed;

	public delegate void LiftHandler();
	public event LiftHandler ItemLifted;

	// Start is called before the first frame update
	void Awake()
    {
		rb = GetComponent<Rigidbody>();
		GetComponent<PlayerData>().TraitUpdated += OnTraitUpdated;
		collider = GetComponentInChildren<Collider>();
    }

	protected void OnTraitUpdated(string s, float v)
	{
		switch (s)
		{
			case "Strength":
				//Debug.Log("strength updated " + v);
				jumpHeight = v;
				lift = 5f * v;
				break;
			case "Speed":
				moveSpeed = v;
				turnSpeed = 20f * v;
				//Debug.Log("speed updated " + v);
				break;
			case "Endurance":
				break;
			case "Breath":
				break;
			default:
				break;
		}
	}

    // Update is called once per frame
    void Update()
    {
		HandleInput();
	}

	private void OnTriggerStay(Collider other)
	{
		if (Input.GetKey(KeyCode.E) && other.tag == "Edible")
		{
			Dictionary<string, TraitInstance> ti = other.GetComponent<ItemInstance>().Traits;
			Destroy(other.transform.gameObject);
			if (ItemConsumed != null)
			{
				ItemConsumed(ti);
			}
		}

		if (Input.GetKey(KeyCode.E) && other.tag == "Liftable")
		{
			if (other.GetComponent<Rigidbody>().mass <= lift)
			{
				Debug.Log("lifted");
				other.GetComponent<Rigidbody>().AddForce(Vector3.up * 10 * lift);
				if (ItemLifted != null)
				{
					ItemLifted();
				}
			}
			
		}
	}



	private void HandleInput()
	{
		float h = Input.GetAxis("Horizontal");
		float v = Input.GetAxis("Vertical");
		Vector3 dir = Vector3.zero;

		if (h != 0f || v != 0f)
		{
			//Debug.Log("should move");
			dir = new Vector3(h, 0f ,v).normalized;
			dir *= moveSpeed;
		}

		CheckGrounded();

		if (Input.GetKeyDown(KeyCode.Space) && isGrounded)
		{
			//Debug.Log("jump");
			//Debug.Log(jumpHeight);
			rb.velocity += Vector3.up * jumpHeight;
		}

		if (Input.GetKeyDown(KeyCode.Return))
		{
			GameSaveData gsd = new GameSaveData();
			gsd.playerPositionX = transform.position.x;
			gsd.playerPositionY = transform.position.y;
			gsd.playerPositionZ = transform.position.z;
			gsd.playerRotationX = transform.rotation.eulerAngles.x;
			gsd.playerRotationY = transform.rotation.eulerAngles.y;
			gsd.playerRotationZ = transform.rotation.eulerAngles.z;
			gsd.playerTraits = new Dictionary<string, TraitInstance>();
			foreach (var item in GetComponent<PlayerData>().PlayerTraits)
			{
				gsd.playerTraits.Add(item.Key,item.Value);
			}
			//
			SaveAndLoad.Save(gsd);

			gsd = SaveAndLoad.Load();
		}

		if (rb.velocity.y < 0f)
		{
			rb.velocity += Vector3.up * Physics.gravity.y * (fallMultiplier - 1) * Time.deltaTime;
			//Debug.Log("here: " + Vector3.up * Physics.gravity.y * (fallMultiplier - 1) * Time.deltaTime);
		}
		else if (rb.velocity.y > 0f && !Input.GetKey(KeyCode.Space))
		{
			rb.velocity += Vector3.up * Physics.gravity.y * (lowJumpMultiplier - 1) * Time.deltaTime;
			//Debug.Log("there: " + Vector3.up * Physics.gravity.y * (lowJumpMultiplier - 1) * Time.deltaTime);
		}





		rb.velocity = new Vector3(dir.x,rb.velocity.y,dir.z);
	}

	void CheckGrounded()
	{
		RaycastHit hit;
		if (Physics.Raycast(collider.transform.position,-Vector3.up, out hit))
		{
			//Debug.Log(hit.distance);
			isGrounded = hit.distance <= collider.bounds.size.y/2f + .2f; 
		}
	}
}
