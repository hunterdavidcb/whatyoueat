﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerData : MonoBehaviour
{
	Dictionary<string, TraitInstance> playerTraits = new Dictionary<string, TraitInstance>();

	public delegate void TraitUpdateHandler(string n, float v);
	public event TraitUpdateHandler TraitUpdated;

	public Dictionary<string, TraitInstance> PlayerTraits
	{
		get { return playerTraits; }
	}

	private void Awake()
	{
		GetComponent<PlayerController>().ItemConsumed += OnItemConsumed;
		//Debug.Log("here");
		NPCTrait[] temp = Resources.LoadAll<NPCTrait>("Scriptables/Traits/");
		//Debug.Log(temp.Length);
		for (int i = 0; i < temp.Length; i++)
		{
			playerTraits.Add(temp[i].TraitName, new TraitInstance(temp[i]));
		}

		foreach (var s in playerTraits.Keys)
		{
			//Debug.Log(playerTraits[s].TraitName);
			if (playerTraits[s].PropertyName != null)
			{
				HandleTraits(playerTraits[s].PropertyName,
				playerTraits[s].PropertyInfluencee, Mathf.Clamp(playerTraits[s].Value,
					playerTraits[s].MinValue, playerTraits[s].MaxValue));
			}
			else
			{
				//Debug.Log("here");
				HandleTraits(playerTraits[s].TraitName.Substring(playerTraits[s].TraitName.IndexOf("_") + 1),
					"", Mathf.Clamp(playerTraits[s].Value,
					playerTraits[s].MinValue, playerTraits[s].MaxValue));
			}
			
		}



	}


	protected void OnItemConsumed(Dictionary<string, TraitInstance> ti)
	{
		foreach (var s in ti.Keys)
		{
			//Debug.Log(ti[s].TraitName);

			if (ti[s].PropertyName != null)
			{
				HandleTraits(ti[s].PropertyName,
				ti[s].PropertyInfluencee, Mathf.Clamp(playerTraits[s].Value + ti[s].Value,
					playerTraits[s].MinValue, playerTraits[s].MaxValue));
			}
			else
			{
				//Debug.Log("here");
				HandleTraits(ti[s].TraitName.Substring(ti[s].TraitName.IndexOf("_") + 1),
					"", Mathf.Clamp(playerTraits[s].Value + ti[s].Value,
					playerTraits[s].MinValue, playerTraits[s].MaxValue));
			}
		}
	}


	void HandleTraits(string t,string sub, float v)
	{
		switch (t)
		{
			case "transform":
				switch(sub)
				{
					case "localScale":
						transform.localScale = new Vector3(v, v, v);
						GetComponent<Rigidbody>().mass = v;
						//Debug.Log(v);
						break;
				}
				break;
			case "Speed":
				if (TraitUpdated != null)
				{
					TraitUpdated("Speed", v);
				}
				break;
			case "Strength":
				if (TraitUpdated != null)
				{
					TraitUpdated("Strength", v);
				}
				break;
			case "Endurance":
				if (TraitUpdated != null)
				{
					TraitUpdated("Endurance", v);
				}
				break;
			case "Breath":
				if (TraitUpdated != null)
				{
					TraitUpdated("Breath", v);
				}
				break;
			default:
				break;
		}
	}


}
