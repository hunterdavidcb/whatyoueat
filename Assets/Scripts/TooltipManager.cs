﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TooltipManager : MonoBehaviour
{
	// Start is called before the first frame update

	static RectTransform rect;
	static Text text;

	private void Awake()
	{
		rect = transform.GetChild(0).GetComponent<RectTransform>();
		text = rect.GetComponentInChildren<Text>();
		rect.gameObject.SetActive(false);
	}

	void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

	public static void OnShowTooltip(Vector2 pos, string m)
	{
		rect.gameObject.SetActive(true);
		text.text = m;

	}

	public static void OnHideTooltip(Vector2 pos, string m)
	{
		rect.gameObject.SetActive(false);
		text.text = "";
	}
}
