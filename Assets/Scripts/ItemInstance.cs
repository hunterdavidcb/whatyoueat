﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemInstance : MonoBehaviour
{
	public Item item;
	Dictionary<string, TraitInstance> traits = new Dictionary<string, TraitInstance>();
	public delegate void TooptipAppearance(Vector2 pos, string m);
	public event TooptipAppearance ShowTooltip;
	public event TooptipAppearance HideTooltip;

	public Dictionary<string, TraitInstance> Traits
	{
		get { return traits; }
	}
    // Start is called before the first frame update
    void Start()
    {
		ShowTooltip += TooltipManager.OnShowTooltip;
		HideTooltip += TooltipManager.OnHideTooltip;
		for (int i = 0; i < item.traits.Count; i++)
		{
			TraitInstance ti = new TraitInstance(item.traits[i].TraitName,
				Random.Range(item.ranges[i].x, item.ranges[i].y), item.traits[i].MinValue,
				item.traits[i].MaxValue, item.traits[i].Property);
			traits.Add(item.traits[i].TraitName, ti);
		}
    }

    // Update is called once per frame
    void Update()
    {
        
    }

	private void OnTriggerEnter(Collider other)
	{
		if (other.tag == "Player")
		{
			//Debug.Log("hey");
			if (ShowTooltip != null)
			{
				ShowTooltip(Vector2.zero, item.name);
			}
		}
	}

	private void OnTriggerExit(Collider other)
	{
		if (other.tag == "Player")
		{
			if (HideTooltip != null)
			{
				HideTooltip(new Vector2(), "");
			}
		}
	}
}
