﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Item")]
public class Item : ScriptableObject
{
	public List<NPCTrait> traits = new List<NPCTrait>();
	public List<Vector2> ranges = new List<Vector2>();
}

