﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NPCTrait")]
public class NPCTrait : ScriptableObject, ITrait
{
	//public
	public string TraitName
	{
		get { return name; }
	}

	//[HideInInspector]
	public float Value
	{
		get { return value; }
		set { this.value = value; }
	}

	public Property Property
	{
		get { return property; }
		set { property = value; }
	}

	public string PropertyName
	{
		get { return property.name; }
	}

	public string PropertyInfluencee
	{
		get { return property.propertyInfluencee; }
	}

	public float MinValue
	{
		get { return minValue; }
		set { minValue = value; }
	}

	public float MaxValue
	{
		get { return maxValue; }
		set { maxValue = value; }
	}

	[SerializeField]
	private Property property;
	[SerializeField]
	private float value;
	[SerializeField]
	private float minValue;
	[SerializeField]
	private float maxValue;


	//

}
