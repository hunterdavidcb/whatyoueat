﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ITrait
{
    string TraitName { get; }
	float Value { get; }
	float MinValue { get; }
	float MaxValue { get; }
	string PropertyName { get; }
	string PropertyInfluencee { get; }
}
