﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
[CreateAssetMenu(fileName = "Property")]
public class Property : ScriptableObject
{
	public string propertyInfluencee;
}
