﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class TraitInstance : ITrait
{
	public string TraitName
	{
		get { return traitName; }
	}

	public float Value
	{
		get { return value; }
	}

	public float MinValue
	{
		get { return minValue; }
	}

	public float MaxValue
	{
		get { return maxValue; }
	}

	public string PropertyName
	{
		get { return propertyName; }
	}

	public string PropertyInfluencee
	{
		get { return propertyInfluencee; }
	}

	private string traitName;
	private float value;
	private float minValue;
	private float maxValue;
	//this is necessary because binary serializers cannot serialize scriptable objects
	private string propertyName;
	private string propertyInfluencee;

	public TraitInstance(string n, float v, float min, float max, Property p)
	{
		traitName = n;
		value = v;
		propertyName = p.name; 
		propertyInfluencee = p.propertyInfluencee;
		minValue = min;
		maxValue = max;
	}

	public TraitInstance(NPCTrait trait)
	{
		traitName = trait.TraitName;
		value = trait.Value;

		if (trait.Property != null)
		{
			propertyName = trait.Property.name;
			propertyInfluencee = trait.Property.propertyInfluencee;
		}

		minValue = trait.MinValue;
		maxValue = trait.MaxValue;
	}
}
